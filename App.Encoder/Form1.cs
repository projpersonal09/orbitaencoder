﻿using System;
using System.Windows.Forms;

namespace App.Encoder
{
    public partial class Form1 : Form
    {
        private Device.Encoder myEncoder = new Device.Encoder();
        private Device.NativeEncoder nativeEncoder = new Device.NativeEncoder();
        private string encoderErr = string.Empty;
        private string encoderHex = string.Empty;
        string cardNo, newCardNo, err, hex, msg, act, dat, res, errs;
        private const string authCode = "938014";

        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            //nativeEncoder.Initialize();
            myEncoder.Disconnect();

            if (!myEncoder.Connect(out encoderErr, out encoderHex))
            {
                SetEncoderStatus(encoderErr, Status.error);
                return;
            }
            SetEncoderStatus("Encoder connect successful.", Status.info);

        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            myEncoder.Disconnect();
            SetEncoderStatus("Encoder disconnected successful.", Status.info);
        }

        private void SetEncoderStatus(string text, Status status)
        {
            lblStatus.Text = text;
        }

        public enum Status
        {
            NULL,
            info,
            warning,
            error
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            act = "Read Card";
            string err, hex;
            string key = string.Empty, roomnumber = string.Empty;
            string[] keys = myEncoder.ReadCard(authCode, out err, out hex);
            if (keys == null)
            {
                rtxDisplay.Clear();
            }
            else
            {
                rtxDisplay.Text = $"Card No: {keys[0]}\nBuidling: {keys[1]}\nRoom: {keys[2]}\nCommon Doors: {keys[3]}\nArrival: {keys[4]}\nDeparture: {keys[5]}";
            }
            lblStatus.Text = err;
            //var cardData = nativeEncoder.ReadCard();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            act = "Delete Card";
            string room;
            if (!myEncoder.DeleteCard(out cardNo, out room, out err, out hex))
            {
                errs = err;
                lblStatus.Text = "Failure";
            }
            else
            {
                lblStatus.Text = "Success";
            }
        }
        private void btnWrite_Click(object sender, EventArgs e)
        {
            try
            {
                var currentDate = DateTime.Now;
                var roomNumber = txtRoom.Text;
                var data = new string[]
                  {
                        Guid.NewGuid().ToString(),
                        "10",
                        authCode,
                        txtBuilding.Text,
                        roomNumber.PadRight(18, '0'),
                        "00000000",
                        currentDate.ToString("yyyy-MM-dd hh:mm:ss"),
                        currentDate.AddDays(1).ToString("yyyy-MM-dd hh:mm:ss")
                  };
                if (!myEncoder.IssueCard(data, out cardNo, out newCardNo, out err, out hex))
                {
                    errs = err;
                    lblStatus.Text = "Failure";
                }
                else
                {
                    lblStatus.Text = "Success";
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils
{
    public class CommDoors
    {
        public static string ChPosition(string original)
        {

            StringBuilder sb = new StringBuilder(original);

            int len = sb.Length;

            int cnt = len / 2;

            char temp;

            for (int i = 0; i < cnt; i++)
            {
                temp = sb[i];

                sb[i] = sb[len - 1 - i];

                sb[len - 1 - i] = temp;
            }

            return sb.ToString();
        }
    }
}

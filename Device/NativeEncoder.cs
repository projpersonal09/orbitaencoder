﻿using System;
using System.Text;

namespace Device
{
    public class NativeEncoder
    {
        public const short PORT_USB = 100;
        public const int BAUD_RATE = 115200;
        private IntPtr icDev;
        protected byte targetSector;

        public static object Program { get; private set; }

        public void Initialize()
        {
            icDev = NativePInvoke.Init(PORT_USB, BAUD_RATE);

            if (icDev == IntPtr.Zero)
            {
                throw new Exception("Unable to find the Card Reader");
            }

            byte version = 0;

            if (NativePInvoke.GetVer(icDev, ref version) != 0)
                throw new Exception("Testing CardReader failed");

            if (NativePInvoke.Beep(icDev, 100) != 0)
                throw new Exception("Testing CardReader beep failed");

        }

        public CardData ReadCard()
        {
            if (icDev == IntPtr.Zero)
                throw new Exception("The card reader has not been initialized");

            Assert(NativePInvoke.Reset(icDev, 0), "Reset card reader status failed");

            ushort tagType = 0;
            Assert(NativePInvoke.Request(icDev, 0, ref tagType), "No card detected");

            uint serial = 0;
            Assert(NativePInvoke.AntiColl(icDev, 0, ref serial), "Detected multiple cards");

            byte size = 0;
            Assert(NativePInvoke.Select(icDev, serial, ref size), "Unable to select card");

            Assert(NativePInvoke.Authentication(icDev, 0, targetSector), "Unable to verify card, May not be a valid");

            StringBuilder builder = new StringBuilder(16);
            Assert(NativePInvoke.Read(icDev, (byte)(targetSector * 4), builder), "Card reading faile");

            string rawData = builder.ToString();

            try
            {
                return new CardData
                {
                    InternalId = serial.ToString("x"),
                    StudentId = rawData.Substring(0, 9),
                    Revision = int.Parse(rawData.Substring(9, rawData.Length > 10 ? 2 : 1).Trim())
                };
            }
            catch
            {
                throw new Exception("Card magnetic area data is incorrect");
            }
        }

        public class CardData
        {
            public string InternalId { get; set; }
            public string StudentId { get; set; }
            public int Revision { get; set; }
        }



        protected static void Assert(short returnValue, string errorMessage)
        {
            if (returnValue != 0)
            {
                var message = (String.Format("Card read error ret {0} ({1})", returnValue, errorMessage));
                throw new Exception(message);
            }
        }
    }
}

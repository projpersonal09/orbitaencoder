﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;
using XmlUtils;

using Utils;
using System.Threading;
using System.Net;
using System.Text.RegularExpressions;

namespace InterfaceClient
{
    public partial class FrmMain : Form
    {
        Device.Encoder myEncoder = new Device.Encoder();
        BollenSocket clientSocket;
        Thread clientThread;
        private bool reconn;
        private Color currConnStaColor;
        private bool exit = false;
        private string encoderErr = string.Empty;
        private string encoderHex = string.Empty;
        public FrmMain()
        {
            InitializeComponent();
            clientSocket = null;
            reconn = true;
        }

        private void EnableEdit(bool edit)
        {
            this.txtKeyCoderName.ReadOnly = !edit;
            this.txtIp.ReadOnly = !edit;
            this.txtPort.ReadOnly = !edit;
        }

        private Dictionary<string, string> CheckKeyContent()
        {
            Dictionary<string, string> content = null;
            string keyCoderName = this.txtKeyCoderName.Text.Trim();
            string ip = this.txtIp.Text.Trim();
            string port = this.txtPort.Text.Trim();
            if (keyCoderName == string.Empty || ip == string.Empty || port == string.Empty)
            {
                SetConnectStatus("The key content cannot be empty.", Status.error);
                return content;
            }

            string pattern = @"\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\b";
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(ip))
            {
                SetConnectStatus("Ip error.", Status.error);
                return content;
            }


            content = new Dictionary<string, string>();
            content.Add("KeyCoderName", keyCoderName);
            content.Add("IP",ip);
            content.Add("Port", port);
            SetConnectStatus("", Status.info);

            return content;
        }

        private void ConnectToServer()
        {
            string workstation = string.Empty;
            reconn = false;
            Dictionary<string,string> content;
       
            if ((content = CheckKeyContent()) == null)
            {
                reconn = true;
                return;
            }
            byte[] data = new byte[1024];
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(content["IP"]), Convert.ToInt32(content["Port"]));
            clientSocket = new BollenSocket(endPoint);
            if (clientSocket.Connect())
            {
                while (true)
                {
                    SocketEntity entity;
                    try
                    {
                        entity = BollenSocket.Receive(clientSocket.NetStream);
                        if (entity == null)
                        {
                            continue;
                        }
                        
                        string cardNo, newCardNo, err, hex, msg, act, dat, res, errs;

                        msg = act = dat = errs = res = string.Empty;;

                        string crlf = "\r\n";

                        workstation = entity.ClientIP;

                        switch(entity.Cmd)
                        {
                            case Command.IF:
                                //workstation = entity.ClientIP;
                                this.Invoke(new DelegateSetConnectStatus(SetConnectStatus), "Connected to ORBITA server.[" + entity.ServerIP + "]", Status.info);
                                BollenSocket.Send(clientSocket.NetStream,
                                    new SocketEntity(
                                        Command.RQ,
                                        new String[] { this.txtKeyCoderName.Text },
                                        "",
                                        ""));
                                break;
                            case Command.KD:  //delete card 
                                act = "Delete Card";
                                string room, priorityRoom = entity.Data[0];
                                if (!myEncoder.DeleteCard(out cardNo, out room, out err, out hex))
                                {
                                    errs = err;
                                    res = "Failure";
                                }
                                else
                                {
                                    res = "Success";
                                }

                                if (priorityRoom != string.Empty)
                                {
                                    room = priorityRoom;
                                }

                                /* key answer data struct
                                 * data[0] key type
                                 * data[1] result
                                 * data[2] workstation
                                 * data[3] card no
                                 * data[4] room
                                 */
                                BollenSocket.Send(
                                    clientSocket.NetStream,
                                    new SocketEntity(
                                        Command.KA,
                                            new string[] {"KD", hex, workstation, cardNo, room},
                                            "", ""
                                ));
                                break;
                            case Command.KR:  //issue card  
                                act = "Issue Card";

                                int cnt = Convert.ToInt32(entity.Data[1]);  //key count

                                int i = 0;
                                
                                int findCardTimeoutFlag = 0;

                                while(i < cnt)
                                {
                                    findCardTimeoutFlag = 0;
                                    while (true)
                                    {
                                        findCardTimeoutFlag++;

                                        if (!myEncoder.FindCard())
                                        {
                                            if (findCardTimeoutFlag < 100)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }  

                                    if (!myEncoder.IssueCard(entity.Data, out cardNo, out newCardNo, out err, out hex))
                                    {
                                        errs = err;
                                        res = "Failure";
                                    }
                                    else
                                    {
                                        res = "Success";
                                    }

                                    dat = crlf + "     Key Count: " + entity.Data[1] + crlf + 
                                          "     Building: " + entity.Data[3] + crlf +
                                          "     Room: " + entity.Data[4] + crlf +
                                          "     Common Doors: " + entity.Data[5] + crlf +
                                          "     Arrival: " + entity.Data[6] + crlf +
                                          "     Departure: " + entity.Data[7];

                                    /* key answer data struct
                                     * data[0] key type
                                     * data[1] result
                                     * data[2] workstation
                                     * data[3] guid
                                     * data[4] pre card no
                                     * data[5] new card no
                                     * data[6] data time
                                     */
                                    BollenSocket.Send(
                                        clientSocket.NetStream,
                                        new SocketEntity(
                                            Command.KA,
                                                new string[] { "KR", hex, workstation, entity.Data[0]/*guid*/, cardNo, newCardNo, DateTime.Now.ToString() },
                                                "", ""
                                    ));

                                    if (cnt > 1)
                                        Thread.Sleep(1000);

                                    i++;
                                }

                                break;
                            case Command.KG: //read card
                                act = "Read Card";
                                string key = string.Empty, roomnumber = string.Empty;
                                string[] keys = myEncoder.ReadCard(entity.Data[0]/*auth*/, out err, out hex);
                                if (keys == null)
                                {
                                    errs = err;
                                    res = "Failure";                                  
                                }
                                else
                                {
                                    res = "Success";
                                    /* read card data struct
                                     * data[0] Card No
                                     * data[1] Building
                                     * data[2] Room
                                     * data[3] Common Doors
                                     * data[4] Arrival
                                     * data[5] Departure
                                     */

                                    string arrival = string.Empty, departure = string.Empty;

                                    DateTime dtArrival = DateTime.ParseExact(keys[4], "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime dtDeparture = DateTime.ParseExact(keys[5], "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                                    roomnumber = keys[2];

                                    key =
                                        "CN" + keys[0] + "#" +
                                        /*"R" + keys[2] + "#" +*/
                                        "*" + "#" +
                                        "CD" + keys[3] + "#" +
                                        "AD" + string.Format("{0:yyyyMMddHHmm}", dtArrival) + "#" +
                                        "DD" + string.Format("{0:yyyyMMddHHmm}",dtDeparture);

                                    dat = crlf + "     Card NO: " + keys[0] + crlf +
                                      "     Building: " + keys[1] + crlf +
                                      "     Room: " + keys[2] + crlf +
                                      "     Common Doors: " + keys[3] + crlf +
                                      "     Arrival: " + keys[4] + crlf +
                                      "     Departure: " + keys[5];
                                }

                                /* key answer data struct
                                 * data[0] key type
                                 * data[1] result
                                 * data[2] workstation
                                 * data[3] room
                                 * data[4] data
                                 */
                                BollenSocket.Send(
                                        clientSocket.NetStream,
                                        new SocketEntity(
                                            Command.KA,
                                                new string[] {"KG", hex, workstation, roomnumber, key},
                                                "", ""
                                ));
                                break;
                            default:
                                break;
                        }

                        if (act != string.Empty)
                        {
                            msg = "[1] Action:  " + act + crlf + crlf +
                                  "[2] Data:  " + dat + crlf + crlf +
                                  "[3] Error:  " + errs + crlf + crlf +
                                  "[4] Result:  " + res;
                            ShowCardStatus(msg);
                        }
                    }
                    catch
                    {
                        this.Invoke(new DelegateSetConnectStatus(SetConnectStatus), "Receive Error.", Status.error);
                        break;
                    }
                }
            }
                
            if (clientSocket == null || !clientSocket.TCPClient.Connected)
            {
                this.Invoke(new DelegateSetConnectStatus(SetConnectStatus), "Not connected to the server.", Status.error);
                reconn = true;
                return;
            }
            
        }

        private void SaveConfigure()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            Dictionary<string,string> content = CheckKeyContent();
            if (content == null)
            {
                MessageBox.Show("The key content cannot be empty.", "hint", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            IList<MyDictionary> list = new List<MyDictionary>();
            list.Add(new MyDictionary { key = "First", value = ""});
            list.Add(new MyDictionary { key = "KeyCoderName", value = content["KeyCoderName"]});
            list.Add(new MyDictionary { key = "IP", value = content["IP"] });
            list.Add(new MyDictionary { key = "Port", value = content["Port"]});
            CMySection mySection = config.GetSection("MySection") as CMySection;
            mySection.KeyValues.Clear();
            (from v in list.AsQueryable()
             select new KeyValueSetting { Key = v.key, Value = v.value }).ToList()
                .ForEach(kv => mySection.KeyValues.Add(kv));
            try
            {
                config.Save();
                MessageBox.Show("Data has been modified.", "hint", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Write data error: " + ex.Message);
                return;
            }
            setEditable(false);
            this.timer1.Enabled = true;
        }

        private void TestEncoder()
        {
            myEncoder.Disconnect();

            if (!myEncoder.Connect(out encoderErr, out encoderHex))
            {
                SetEncoderStatus(encoderErr, Status.error);
                return;
            }
            SetEncoderStatus("Encoder connect successful.", Status.info);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.statusStrip1.Items.Insert(1,new ToolStripSeparator());

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            CMySection mySection = config.GetSection("MySection") as CMySection;
            string lFirst = mySection.KeyValues["First"].Value;
            this.txtKeyCoderName.Text = mySection.KeyValues["KeyCoderName"].Value;
            this.txtIp.Text = mySection.KeyValues["IP"].Value;
            this.txtPort.Text = mySection.KeyValues["Port"].Value;

            if (lFirst == "yes")
            {
                setEditable(true);
                this.timer1.Enabled = false;
            }
            else
            {
                setEditable(false);
                this.timer1.Enabled = true;
            }

            if (myEncoder.Connect(out encoderErr, out encoderHex))
            {
                SetEncoderStatus("Encoder connect successful.", Status.info);
            }
            else
            {
                SetEncoderStatus(encoderErr, Status.error);
            }
        }

        private void SetEncoderStatus(string text,Status sta)
        {
            Image image = null;
            Color currColor;
            
            ToolStripStatusLabel tssl = this.toolStripStatusLabelEncoder;
            switch (sta)
            {
                case Status.info:
                    currColor = Color.Green;
                    image = this.imageList1.Images[0];
                    break;
                case Status.error:
                    currColor = Color.Red;
                    image = this.imageList1.Images[1];
                    break;
                default:
                    currColor = Color.Black;
                    image = null;
                    break;
            }

            tssl.Text = text;
            tssl.ForeColor = currColor;
            tssl.Image = image;
        }

        delegate void DelegateSetConnectStatus(string text, Status sta);
        private void SetConnectStatus(string text, Status sta)
        {
            Image image = null;
            ToolStripStatusLabel tssl = this.toolStripStatusLabelNetwrok;
            switch (sta)
            {
                case Status.info:
                    currConnStaColor = Color.Green;
                    image = this.imageList1.Images[2];
                    break;
                case Status.error:
                    currConnStaColor = Color.Red;
                    image = this.imageList1.Images[3];
                    break;
                default:
                    currConnStaColor = Color.Black;
                    image = null;
                    break;
            }
           
            tssl.Text = text;
            //tssl.ForeColor = currColor;
            tssl.Image = image;
        }

        delegate void ShowCardStatusHandler(string text);
        private void ShowCardStatus(string text)
        {
            if (this.txtStatus.InvokeRequired)
            {
                ShowCardStatusHandler d = new ShowCardStatusHandler(ShowCardStatus);
                this.Invoke(d, new Object[] { text });
            }
            else
            {
                this.txtStatus.Text = text;
            }
        }

        private bool PingHost(string addr, int timeout = 1000)
        {
            using (System.Net.NetworkInformation.Ping pingSender = new System.Net.NetworkInformation.Ping())
            {
                PingOptions options = new PingOptions();
                options.DontFragment = true;
                string Data = "testing...";
                byte[] DataBuffer = Encoding.ASCII.GetBytes(Data);
                PingReply reply = pingSender.Send(addr, timeout, DataBuffer, options);
                if (reply.Status == IPStatus.Success)
                    return true;

                return false;
            }

        }

        private void btmTestLink_Click(object sender, EventArgs e)
        {
            string host = this.txtIp.Text.Trim();
            if (host == string.Empty)
            {
                MessageBox.Show("The IP cannot be empty.","hint",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            if (PingHost(host))
            {
                MessageBox.Show("Link success.", "hint", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Link failed.", "hint", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setEditable(bool edit)
        {
            this.txtKeyCoderName.ReadOnly = !edit;
            this.txtIp.ReadOnly = !edit;
            this.txtPort.ReadOnly = !edit;
            this.toolStripButtonSave.Enabled = edit;
        }

        private void Reset()
        {
            setEditable(true);
            this.timer1.Enabled = false;
            SetConnectStatus("", Status.NULL);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAbout frmAbout = new FrmAbout();
            frmAbout.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Exit()
        {
            exit = true;
            this.Close();
        }

        private void MaximumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timer1.Interval = 5000;
            if (!reconn)
            {
                return;
            }
            clientThread = new Thread(ConnectToServer);
            clientThread.Name = "Client Thread";
            clientThread.IsBackground = true;
            clientThread.Start();
        }

        private void toolStripStatusLabelNetwrok_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.toolStripStatusLabelNetwrok.ForeColor = currConnStaColor;
            }
            catch
            {

            }
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }          
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            myEncoder.Disconnect();
        }

        private void toolStripButtonTestEncoder_Click(object sender, EventArgs e)
        {
            TestEncoder();
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            SaveConfigure();
        }

        private void toolStripButtonReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            Exit();
        }

        private void toolStripButtonAbout_Click(object sender, EventArgs e)
        {
            FrmAbout frmAbout = new FrmAbout();
            frmAbout.ShowDialog();
        }

        private void txtStatus_TextChanged(object sender, EventArgs e)
        {
            if (txtStatus.Text.Contains("Fail"))
            {
                txtStatus.ForeColor = Color.Red;
            }
            else
            {
                txtStatus.ForeColor = Color.ForestGreen;
            }
        }

    }

    class MyDictionary
    {
        public string key;
        public string value;
    }

    public enum Status
    {
        NULL,
        info,
        warning,
        error
    }
}
